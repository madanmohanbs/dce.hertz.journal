﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Excel;
using Microsoft.Office.Tools;

namespace DCE.Hertz.Journal.ExcelAddIn
{
    public partial class ThisAddIn
    {
        private TaskPaneControl taskPaneControl1;
        private Microsoft.Office.Tools.CustomTaskPane taskPane;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {

            //taskPaneControl1 = new TaskPaneControl();
            //taskPaneValue = this.CustomTaskPanes.Add(taskPaneControl1, "MyCustomTaskPane");
            //taskPaneValue.VisibleChanged += new EventHandler(taskPaneValue_VisibleChanged);
            Application.WindowActivate += Application_WindowActivate;
            Application.WorkbookOpen += Application_WorkbookOpen;
            Application.WorkbookBeforeClose += Application_WorkbookBeforeClose;    
           
        }

        private void Application_WorkbookBeforeClose(Excel.Workbook Wb, ref bool Cancel)
        {
            CustomTaskPane removedItem = null;
            foreach (CustomTaskPane item in CustomTaskPanes)
            {
                var item_window = item.Window as Excel.Window;
                var wb_window = Wb.Windows.Application.ActiveWindow  as Excel.Window;
                if (item_window.Hwnd == wb_window.Hwnd)
                {
                    removedItem = item;
                    break;
                }
            }
            if(removedItem!=null)
            {
                CustomTaskPanes.Remove(removedItem);
            }
        }

        void Application_WorkbookOpen(Excel.Workbook Wb)
        {
            JournalRibbon ribbon = Globals.Ribbons.GetRibbon(typeof(JournalRibbon)) as JournalRibbon;
            //ribbon.ResetButtons();
            
        }

        void Application_WindowActivate(Excel.Workbook Wb, Excel.Window Wn)
        {


            JournalRibbon ribbon = Globals.Ribbons.GetRibbon(typeof(JournalRibbon)) as JournalRibbon;
           
            

            try
            {
                foreach (CustomTaskPane item in CustomTaskPanes)
                {
                    if (item != null || item.Window != null)
                    {
                        var window = item.Window as Excel.Window;
                        if (window.Hwnd == Wn.Hwnd)
                        {
                            taskPane = item;
                            ribbon.X = (TaskPaneControl)taskPane.Control;
                            ribbon.ResetButtons();
                            return;
                        }
                    }
                }
                
                taskPaneControl1 = new TaskPaneControl();
                ribbon.X = taskPaneControl1;
                taskPane = CustomTaskPanes.Add(taskPaneControl1, "Hertz Journal Creation", Wn);
                //taskPane.Visible = true;
                ribbon.ResetButtons();
                taskPane.VisibleChanged += pane_VisibleChanged;
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message, "Window Activate");
            }
        }

        void pane_VisibleChanged(object sender, EventArgs e)
        {
            CustomTaskPane pane = sender as CustomTaskPane;
            //if (pane.Visible == false)
            //{
            //    CustomTaskPanes.Remove(pane);
            //    pane.VisibleChanged -= new EventHandler(pane_VisibleChanged);

            //}
            Globals.Ribbons.JournalRibbon.btnTaskPane.Checked = !pane.Visible;
        }
        //private void taskPaneValue_VisibleChanged(object sender, System.EventArgs e)
        //{
        //    if (taskPaneValue.Visible == false)
        //    {
        //        CustomTaskPanes.Remove(taskPaneValue);
        //        taskPaneValue.VisibleChanged -= new EventHandler(taskPaneValue_VisibleChanged);
        //        taskPaneValue = null;
        //    }

        //    Globals.Ribbons.JournalRibbon.btnTaskPane.Checked = taskPaneValue.Visible;
        //}
        public Microsoft.Office.Tools.CustomTaskPane TaskPane
        {
            get
            {
                return taskPane;
            }
        }
        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {

        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);

        }

        #endregion
    }
}
