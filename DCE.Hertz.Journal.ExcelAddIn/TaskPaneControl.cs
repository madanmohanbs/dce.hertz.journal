﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data.OleDb;

namespace DCE.Hertz.Journal.ExcelAddIn
{
    public partial class TaskPaneControl : UserControl
    {
        
        public Admin Admin = null;
        public ProcessData ProcessData = null;
        public Process.Helper Helper = null;
        public DataSet ExcelDataSet = null;
       
        public TaskPaneControl()
        {
            InitializeComponent();
            Admin = new Admin();
            ProcessData = new ProcessData();
            Helper = new Process.Helper(Admin, ProcessData, ExcelDataSet); ;
        }
        private void TaskPaneControl_Load(object sender, EventArgs e)
        {

            InitControls();

        }

        public void InitControls()
        {
            

            ProcessData = Helper.UpdateAllSheetNames(ProcessData);


            InitDropDownControl(cmbLogicSheet, ProcessData.LogicSheet);

            InitDropDownControl(cmbChqSheet, ProcessData.ChequeSheet);


            InitDropDownControl(cmbJournalSheet, ProcessData.JournalSheet);

            InitDropDownControl(cmbErrorSheet, ProcessData.ErrorSheet);

            chkListBankTabs.Items.Clear();
            foreach (string tab in ProcessData.AllSheets.ToArray())
            {
                bool belongToOthers = (ProcessData.LogicSheet.IndexOf(tab, StringComparison.OrdinalIgnoreCase) >= 0 || ProcessData.JournalSheet.IndexOf(tab, StringComparison.OrdinalIgnoreCase) >= 0 || ProcessData.ErrorSheet.IndexOf(tab, StringComparison.OrdinalIgnoreCase) >= 0 || ProcessData.ChequeSheet.IndexOf(tab, StringComparison.OrdinalIgnoreCase) >= 0);//|| tab == ProcessData.SampleSheet || (ProcessData.MoveTabSheets != null && ProcessData.MoveTabSheets.Contains(tab, StringComparer.InvariantCultureIgnoreCase)));
                if (!belongToOthers)
                {
                    chkListBankTabs.Items.Add(tab);
                }
            }

            if (ProcessData.BankSheets != null && ProcessData.BankSheets.Count > 0)
            {
                for (int i = 0; i < chkListBankTabs.Items.Count; i++)
                {
                    if (ProcessData.BankSheets.Contains(chkListBankTabs.Items[i].ToString(), StringComparer.OrdinalIgnoreCase))
                        chkListBankTabs.SetItemChecked(i, true);
                }
            }
            chkListAttachments.Items.Clear();
            chkListAttachments.Items.AddRange(ProcessData.AllSheets.ToArray());


        }

        private void InitDropDownControl(ComboBox cmbBox, string defaultValue)
        {
            cmbBox.DataSource = ProcessData.AllSheets.ToArray();
            if (!string.IsNullOrEmpty(defaultValue))
            {
                if (ProcessData.AllSheets.Exists(s => s.IndexOf(defaultValue, StringComparison.OrdinalIgnoreCase) >= 0))
                {
                    cmbBox.Text = defaultValue;
                }
                else if (defaultValue.Contains(' '))
                {
                    foreach (string str in defaultValue.Split(' '))
                    {

                        if (ProcessData.AllSheets.Exists(s => s.IndexOf(str.Trim(), StringComparison.OrdinalIgnoreCase) >= 0))
                        {
                            cmbBox.Text = ProcessData.AllSheets.Where(s => s.IndexOf(str.Trim(), StringComparison.OrdinalIgnoreCase) >= 0).First();
                            break;
                        }
                    }
                }
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
           
            if (ValidateChildren(ValidationConstraints.Enabled))
            {
                ProcessData.LogicSheet = cmbLogicSheet.Text;

                ProcessData.ChequeSheet = cmbChqSheet.Text;
                //ProcessData.BankSheetRange = txtStatementRange.Text;

                List<string> sheets = new List<string>();
                foreach (var item in chkListBankTabs.CheckedItems)
                {
                    sheets.Add(item.ToString());
                }
                ProcessData.BankSheets = sheets;

                ProcessData.JournalSheet = cmbJournalSheet.Text;

                ProcessData.ErrorSheet = cmbErrorSheet.Text;

               
            }
        }

        private void btnStatementRange_Click(object sender, EventArgs e)
        {
            //string sheetName = cmbChqSheet.Text;
            //Excel.Worksheet activeWorksheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(sheetName);
            //string range = Process.Helper.GetUsedRangeAddress(activeWorksheet);
            //activeWorksheet.Select();


            //txtStatementRange.Text = range;

            //Utilities.NamedRange nr = Utilities.NamedRange.ExtractNamedRanges(txtStatementRange.Text);
            //activeWorksheet.Range[nr.StartColumn + nr.StartRow, nr.EndColumn + nr.EndRow].Select();


        }

        private void cmbWorkSheets_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(cmbLogicSheet.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(cmbLogicSheet, "Select a Sheet");
            }
            else
            {
                e.Cancel = false;
                errorProvider1.Clear();
            }
        }

        private void cmbLogicSheets_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sheetName = cmbLogicSheet.Text;
            if (!string.IsNullOrEmpty(sheetName))
            {
                Excel.Worksheet activeWorksheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(sheetName);
                activeWorksheet.Select();
            }
        }

        private void cmbBankSheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sheetName = cmbChqSheet.Text;
            if (!string.IsNullOrEmpty(sheetName))
            {
                Excel.Worksheet activeWorksheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(sheetName);
                activeWorksheet.Select();
            }
        }

        private void cmbJournalSheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sheetName = cmbJournalSheet.Text;
            if (!string.IsNullOrEmpty(sheetName))
            {
                Excel.Worksheet activeWorksheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(sheetName);
                activeWorksheet.Select();
            }
        }

        private void btnEmailAttachment_Click(object sender, EventArgs e)
        {
            List<string> attachments = new List<string>();
            foreach (var item in chkListAttachments.CheckedItems)
            {
                attachments.Add(item.ToString());
            }
            ProcessData.AttachmentSheets = attachments;
        }


        private void btnRefresh_Click(object sender, EventArgs e)
        {
            InitControls();
        }



    }
}
