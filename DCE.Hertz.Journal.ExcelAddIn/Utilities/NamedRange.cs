﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DCE.Hertz.Journal.ExcelAddIn.Utilities
{
    public class NamedRange
    {

        public string StartColumn { get; set; }
        public int StartRow { get; set; }
        public string EndColumn { get; set; }
        public int? EndRow { get; set; }

        public static NamedRange ExtractNamedRanges(string RangeAddress)
        {

           
            string range = RangeAddress;
            string[] rangeArray = range.Split('$');
            string startCol = rangeArray[1];
            int startRow = int.Parse(rangeArray[2].TrimEnd(':'));
            string endCol = null;
            int? endRow = null;
            if (rangeArray.Length > 3)
            {
                endCol = rangeArray[3];
                endRow = int.Parse(rangeArray[4]);
            }
            NamedRange nr = new NamedRange {  StartRow = startRow, StartColumn = startCol, EndRow = endRow, EndColumn = endCol };


            return nr;
        }
    }
}
