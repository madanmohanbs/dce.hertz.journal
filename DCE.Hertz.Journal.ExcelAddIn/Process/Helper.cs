﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace DCE.Hertz.Journal.ExcelAddIn.Process
{
    public class Helper
    {
        Admin Admin = null;
        ProcessData ProcessData = null;
        DataSet ExcelDataSet = null;
        public Helper(Admin admin, ProcessData processData, DataSet excelDataSet)
        {
            Admin = admin;
            ProcessData = processData;
            ExcelDataSet = excelDataSet;
        }
        public /*static*/ProcessData UpdateAllSheetNames(ProcessData ProcessData)
        {
            Excel.Sheets worksheets = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets;
            List<string> allWorksheets = new List<string>();
            foreach (Excel.Worksheet sheet in worksheets)
            {
                allWorksheets.Add(sheet.Name);
            }
            ProcessData.AllSheets = allWorksheets;
            return ProcessData;
        }
        public /*static*/ DataTable ReadExcelSheet(string sheetName, string range)
        {

            Excel.Worksheet workSheet = string.IsNullOrEmpty(sheetName) ? Globals.ThisAddIn.Application.ActiveWorkbook.ActiveSheet : Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(sheetName);
            string address = string.IsNullOrEmpty(range) ? workSheet.UsedRange.Address : range;


            Utilities.NamedRange nr = Utilities.NamedRange.ExtractNamedRanges(address);

            DataTable dt = null;
            List<string> items = new List<string>();
            for (int rowIndex = nr.StartRow; rowIndex <= nr.EndRow; rowIndex++)
            {
                Excel.Range cellA = workSheet.Cells[rowIndex, 1];
                double notEmpty = Globals.ThisAddIn.Application.WorksheetFunction.CountA(cellA.EntireRow);
                if (notEmpty == 0) //REMOVE EMPTY ROWS!!
                    continue;
                if (rowIndex == nr.StartRow)
                {
                    //create datatable
                    dt = new DataTable();
                    dt.TableName = workSheet.Name;
                    dt.Columns.Add("RowNum", typeof(int));
                }

                items.Clear();
                items.Add(rowIndex.ToString());
                for (uint columnIndex = nr.StartColumn.ToCharArray()[0] - 64U; columnIndex <= nr.EndColumn.ToCharArray()[0] - 64U; columnIndex++)
                {
                    Excel.Range cell = workSheet.Cells[rowIndex, columnIndex];

                    if (rowIndex == nr.StartRow)
                    {
                        string cellValue = ((cell != null) && (cell.Value != null)) ? cell.Value.ToString().Trim() : "Column" + columnIndex;
                        dt.Columns.Add(cellValue);
                    }
                    else
                    {
                        string cellValue = ((cell != null) && (cell.Value != null)) ? cell.Value.ToString().Trim() : null;
                        items.Add(cellValue);
                    }

                }
                if (rowIndex != nr.StartRow)
                {
                    dt.Rows.Add(items.ToArray());
                }

            }

            return dt;
        }

        public /*static*/ String ExceltoCsv(string sheetName, Excel.Range range)
        {
            StringBuilder sb = new StringBuilder();
            Excel.Worksheet activeWorksheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(sheetName);
            Excel.Range activeRange = range == null ? activeWorksheet.UsedRange : range;
            string address = activeRange.Address;

            Utilities.NamedRange nr = Utilities.NamedRange.ExtractNamedRanges(address);

            for (int rowIndex = nr.StartRow; rowIndex <= nr.EndRow; rowIndex++)
            {
                for (int columnIndex = nr.StartColumn.ToCharArray()[0] - 64; columnIndex <= nr.EndColumn.ToCharArray()[0] - 64; columnIndex++)
                {
                    Excel.Range cell = activeWorksheet.Cells[rowIndex, columnIndex];
                    if ((cell != null) && (cell.Value != null))
                    {
                        string cellValue = cell.Value.ToString();
                        cellValue = cellValue.Contains(",") ? String.Format("\"{0}\", ", cellValue) : cellValue + ", ";
                        sb.Append(cellValue);
                    }
                }
                sb.Append("\n");
            }
            return sb.ToString();
        }

        public /*static*/ String ExceltoHtml(string sheetName, Excel.Range range)
        {
            StringBuilder sb = new StringBuilder(); StringBuilder tb = new StringBuilder();
            Excel.Worksheet activeWorksheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(sheetName);
            Excel.Range activeRange = range == null ? activeWorksheet.UsedRange : range;
            string address = activeRange.Address;

            Utilities.NamedRange nr = Utilities.NamedRange.ExtractNamedRanges(address);

            for (int rowIndex = nr.StartRow; rowIndex <= nr.EndRow; rowIndex++)
            {
                tb.Clear();
                for (int columnIndex = nr.StartColumn.ToCharArray()[0] - 64; columnIndex <= nr.EndColumn.ToCharArray()[0] - 64; columnIndex++)
                {

                    Excel.Range cell = activeWorksheet.Cells[rowIndex, columnIndex];
                    if ((cell != null) && (cell.Value != null))
                    {
                        string cellValue = cell.Value.ToString();
                        sb.Append("<td>" + cellValue + "</td>");
                    }
                }
                sb.Append("<tr>" + tb.ToString() + "</tr>");
            }
            return "<table>" + sb.ToString() + "</table>";
        }

        public /*static*/ DataSet LoadSheet(ProcessData processData)
        {
           
            ProcessData = processData;
            this.UpdateAllSheetNames(ProcessData);
            DataSet ds = new DataSet();

            if (!string.IsNullOrEmpty(ProcessData.LogicSheet) && ProcessData.AllSheets.Contains(ProcessData.LogicSheet, StringComparer.OrdinalIgnoreCase))
            {
                DataTable LogicTab = this.ReadExcelSheet(ProcessData.LogicSheet, null);
                ds.Tables.Add(LogicTab);
            }
            else
            {
                MessageBox.Show("Please select the 'Business Logic' sheet in the task pane");
               
            }
            if (ProcessData.BankSheets != null && ProcessData.BankSheets.Count > 0)
            {
                foreach (string bankSheet in ProcessData.BankSheets)
                {
                    if (!string.IsNullOrEmpty(bankSheet) && ProcessData.AllSheets.Contains(bankSheet, StringComparer.OrdinalIgnoreCase))
                    {
                        DataTable BankTab = this.ReadExcelSheet(bankSheet, ProcessData.BankSheetRange);
                        ds.Tables.Add(BankTab);
                    }
                }
            }
            else
            {
                MessageBox.Show("Please select the Bank statement sheet in the task pane");
               
            }
            ExcelDataSet = ds;
            return ExcelDataSet;

        }
        public /*static*/ List<string> ValidateStatement(string sheetName)
        {
            int matched = 0;
            List<string> missingTabs = new List<string>();
            this.UpdateAllSheetNames(ProcessData);
            Excel.Worksheet worksheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(sheetName);
            worksheet.Select();
            //Column Name with Unique value,	Unique Value,	Column Name with Unique value 2,	Unique Value 2,	Tab to Move To,	Prime,	Sub,	cc,	act,	co,	div,	Debit,	Credit,	Line description	
            DataTable LogicTab = ExcelDataSet.Tables[ProcessData.LogicSheet];
            DataTable BankTab = ExcelDataSet.Tables[worksheet.Name];
            foreach (DataRow dr in LogicTab.Rows)
            {
                string firstColumn = dr[Admin.FirstColumn].ToString();
                string secondColumn = dr[Admin.SecondColumn].ToString();
                string firstValue = dr[Admin.FirstValue].ToString();
                string secondValue = dr[Admin.SecondValue].ToString();
                // secondValue =secondValue.Contains(' ')? secondValue.Replace(' ','_'):secondValue;
                string tabName = dr[Admin.TabToMove].ToString();

                string queryString = string.IsNullOrEmpty(secondColumn) ? string.Format("[{0}] LIKE '%{1}%'", firstColumn, firstValue) : string.Format("[{0}] LIKE'%{1}%' AND [{2}] LIKE '%{3}%' ", firstColumn, firstValue, secondColumn, secondValue);


                //BankTab.AsDataView().RowFilter = queryString;
                DataRow[] selectedRows = BankTab.Select(queryString);
                if (selectedRows.Length > 0)
                {
                    matched +=selectedRows.Length;
                    //Highlight
                    foreach (DataRow row in selectedRows)
                    {
                        //Excel.Range cell = activeWorksheet.get_Range("A" + row["RowNum"] , "I" + row["RowNum"]);
                        Excel.Range range = worksheet.get_Range("A" + row["RowNum"]).EntireRow;
                        range.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green);


                        try
                        {
                            Excel.Range cell = worksheet.get_Range("A" + row["RowNum"]);
                            cell.Validation.Delete();
                            cell.Validation.Add(Excel.XlDVType.xlValidateList, Excel.XlDVAlertStyle.xlValidAlertStop, Excel.XlFormatConditionOperator.xlBetween, "0", Type.Missing);
                            cell.Validation.IgnoreBlank = true;
                            cell.Validation.InputTitle = "Move to sheet " + tabName;
                            cell.Validation.InputMessage = queryString;
                        }
                        catch (System.Runtime.InteropServices.COMException ex)
                        {

                        }

                        if (ProcessData.AllSheets != null && ProcessData.AllSheets.Contains(tabName, StringComparer.OrdinalIgnoreCase))
                        {
                            Excel.Worksheet tabSheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(tabName);
                            Excel.Range usedRange = tabSheet.UsedRange;
                            Utilities.NamedRange used = Utilities.NamedRange.ExtractNamedRanges(usedRange.Address);
                            //tabSheet.get_Range(used.StartColumn + (used.EndRow + 1)).EntireRow.Value = range.Value;
                            range.Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green);
                            //range.ClearContents();
                        }
                        else
                        {
                            missingTabs.Add(tabName);
                            range.Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Orange);
                        }

                    }
                }
            }
            MessageBox.Show(string.Format("{0} items matched out of {1} line items in sheet {2}", matched, BankTab.Rows.Count, sheetName));
            return missingTabs.Distinct().ToList();



        }


        public /*static*/ void SortStatement(string sheetName)
        {

            List<string> missingTabs = new List<string>();
            this.UpdateAllSheetNames(ProcessData);
            Excel.Worksheet worksheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(sheetName);
            worksheet.Select();
            //Column Name with Unique value,	Unique Value,	Column Name with Unique value 2,	Unique Value 2,	Tab to Move To,	Prime,	Sub,	cc,	act,	co,	div,	Debit,	Credit,	Line description	
            DataTable LogicTab = ExcelDataSet.Tables[ProcessData.LogicSheet];
            DataTable BankTab = ExcelDataSet.Tables[worksheet.Name];
            foreach (DataRow dr in LogicTab.Rows)
            {
                string firstColumn = dr[Admin.FirstColumn].ToString();
                string secondColumn = dr[Admin.SecondColumn].ToString();
                string firstValue = dr[Admin.FirstValue].ToString();
                string secondValue = dr[Admin.SecondValue].ToString();
                string tabName = dr[Admin.TabToMove].ToString();
                if (!string.IsNullOrEmpty(firstColumn))
                {
                    string queryString = string.IsNullOrEmpty(secondColumn) ? string.Format("[{0}] LIKE '%{1}%'", firstColumn, firstValue) : string.Format("[{0}] LIKE '%{1}%' AND [{2}] LIKE '%{3}%' ", firstColumn, firstValue, secondColumn, secondValue);


                    //BankTab.AsDataView().RowFilter = queryString;
                    DataRow[] selectedRows = BankTab.Select(queryString);
                    if (selectedRows.Length > 0)
                    {
                        //Highlight
                        foreach (DataRow row in selectedRows)
                        {
                            //Excel.Range cell = activeWorksheet.get_Range("A" + row["RowNum"] , "I" + row["RowNum"]);
                            Excel.Range range = worksheet.get_Range("A" + row["RowNum"]).EntireRow;

                            //Tran_Date	 Account_No	 Segment_Id	 Amount	 Description	 Serial	Prime	Sub	CC
                            //TRAN_DATE	 ACCOUNT_NO	 SEGMENT_ID	 CCY	 CLOSING_BAL	 AMOUNT	 TRAN_CODE	 NARRATIVE	 SERIAL
                            //row["Tran_Date"], row["Account_No"], row["Segment_Id"],
                            if (row["Tran_Date"] != null)
                                ProcessData.BankStatementDate = row["Tran_Date"].ToString();
                            string linDescription = dr["Line description"].ToString().Replace("column value", ":");
                            string descriptionColumn = linDescription.Contains(":") ? linDescription.Split(':')[0].Trim() : "";
                            string description = (string.IsNullOrEmpty(descriptionColumn) ? "..." : row[descriptionColumn]) + (linDescription.Contains(":") ? linDescription.Split(':')[1] : "");

                            object[] values = { description, row["Amount"], row["Serial"], dr["Prime"], dr["Sub"], dr["Cc"], dr["RowNum"] };

                            Excel.Worksheet tabSheet = null; ;

                            if (ProcessData.AllSheets != null && ProcessData.AllSheets.Contains(tabName, StringComparer.OrdinalIgnoreCase))
                            {
                                tabSheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(tabName);

                                Excel.Range usedRange = tabSheet.UsedRange;
                                Utilities.NamedRange used = Utilities.NamedRange.ExtractNamedRanges(usedRange.Address);
                                if (used.EndRow == null)
                                {
                                    tabSheet.get_Range("A1", "G1").Value = Admin.NewTabHeading;
                                    tabSheet.get_Range("A2", "G2").Value = values;
                                    row.Delete();
                                    range.ClearContents();
                                }
                                else
                                {
                                    tabSheet.get_Range(used.StartColumn + (used.EndRow + 1), used.EndColumn + (used.EndRow + 1)).Value = values;
                                    row.Delete();
                                    range.ClearContents();
                                }

                            }
                            else
                            {
                                missingTabs.Add(tabName);
                                range.Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                            }



                        }
                    }
                }
            }
            if (missingTabs.Count > 0)
            {
                string messsage = string.Join(",", missingTabs.Distinct().ToArray());
                MessageBox.Show(messsage + " Tabs " + (missingTabs.Count > 1 ? "are" : "is") + " missing");
            }


        }

        public /*static*/ void CreateJournal(string sheetName)
        {

            this.UpdateAllSheetNames(ProcessData);

            DataSet ds = new DataSet();
            DataTable dtLogic = ExcelDataSet.Tables[ProcessData.LogicSheet];
            foreach (string newTabSheet in ProcessData.MoveTabSheets)
            {
                if (ProcessData.AllSheets != null && ProcessData.AllSheets.Contains(newTabSheet, StringComparer.OrdinalIgnoreCase))
                {
                    DataTable dtNewTabSheet = this.ReadExcelSheet(newTabSheet, null);
                    if (dtNewTabSheet != null)
                    {
                        ds.Tables.Add(dtNewTabSheet);
                    }
                }
            }


            if (ProcessData.AllSheets != null && ProcessData.AllSheets.Contains(sheetName, StringComparer.OrdinalIgnoreCase))
            {
                Excel.Worksheet tabSheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(sheetName);
                tabSheet.Select();
                int? rowNum = null;
                foreach (DataTable table in ds.Tables)
                {
                    if (table.TableName == ProcessData.ChequeSheet)
                        continue;
                    Excel.Range usedRange2 = tabSheet.UsedRange;
                    Utilities.NamedRange used2 = Utilities.NamedRange.ExtractNamedRanges(usedRange2.Address);
                    //usedRange2.Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Blue);
                    rowNum = used2.EndRow + 1;
                    int beginRowNum = rowNum.Value;
                    tabSheet.get_Range("J" + rowNum).Value = table.TableName;

                    //decimal TotalAmount = table.Compute("SUM()")
                    foreach (DataRow row in table.Rows)
                    {
                        decimal Debit = 0, Credit = 0, amount = 0;
                        string logicNo = table.Columns.Contains(Admin.LogicRefColumn) ? row[Admin.LogicRefColumn].ToString() : "";
                        string queryString = string.Format("RowNum='{0}'", logicNo);
                        if (!string.IsNullOrEmpty(logicNo))
                        {
                            DataRow dr = dtLogic.Select(queryString).FirstOrDefault();
                            if (dr != null)
                            {
                                string businessLogicDebit = string.IsNullOrEmpty(dr[Admin.DebitFormulaColumn].ToString()) ? "" : "=" + dr[Admin.DebitFormulaColumn].ToString();
                                string businessLogicCredit = string.IsNullOrEmpty(dr[Admin.CreditFormulaColumn].ToString()) ? "" : "=" + dr[Admin.CreditFormulaColumn].ToString();
                                string strAmount = row["Amount"].ToString();


                                //Excel.Range usedRange = tabSheet.UsedRange;
                                //Utilities.NamedRange used = Utilities.NamedRange.ExtractNamedRanges(usedRange.Address);
                                //int? rowNum = used.EndRow + 1;
                                rowNum++;


                                //Upl	Prime	Sub	CC	Act	Co	Div	Debit	Credit	Line Description	Messages
                                object[] values = { dr["Prime"], dr["Sub"], dr["CC"], dr["Act"], dr["Co"], dr["Div"], businessLogicDebit.Replace("Amt", "J" + rowNum), businessLogicCredit.Replace("Amt", "J" + rowNum), row["Description"], row["Amount"] };


                                //tabSheet.get_Range(used.StartColumn + (used.EndRow + 1), used.EndColumn + (used.EndRow + 1)).Value = values;
                                tabSheet.get_Range(used2.StartColumn + rowNum, used2.EndColumn + rowNum).Value = values;

                            }
                        }
                    }
                    //Summarize
                    if (ProcessData.SummaryTabs.Contains(table.TableName, StringComparer.OrdinalIgnoreCase))
                    {
                        tabSheet.Cells[rowNum + 1, 7].Formula = string.Format("=SUM(G{0}:G{1})", beginRowNum, rowNum); //G
                        tabSheet.Cells[rowNum + 1, 8].Formula = string.Format("=SUM(H{0}:H{1})", beginRowNum, rowNum); //H
                        double debit = tabSheet.Cells[rowNum + 1, 7].Value;
                        double credit = tabSheet.Cells[rowNum + 1, 8].Value;
                        tabSheet.get_Range("G" + (beginRowNum + 1), "J" + (beginRowNum + 1)).Value = new object[] { "", "", "", "" };
                        //if (string.Equals(table.TableName, "VIS", StringComparison.OrdinalIgnoreCase))
                        {
                            double vTotal = debit - credit;
                            if (vTotal > 0)
                                tabSheet.Cells[beginRowNum + 1, 7].Value = vTotal;
                            else
                                tabSheet.Cells[beginRowNum + 1, 8].Value = -vTotal;
                        }
                        //else
                        //{
                        //    tabSheet.Cells[beginRowNum + 1, 7].Value = debit;
                        //    tabSheet.Cells[beginRowNum + 1, 8].Value = credit;
                        //}
                        tabSheet.get_Range(used2.StartColumn + (beginRowNum + 2), used2.EndColumn + (rowNum + 1)).Delete(Excel.XlDeleteShiftDirection.xlShiftUp);
                    }
                }
                //HCC
                DataTable hccTabSheet = this.ReadExcelSheet(ProcessData.ErrorSheet, null);
                double hccTotal = 0;
                if (hccTabSheet != null && hccTabSheet.Rows.Count > 0)
                {
                    ds.Tables.Add(hccTabSheet);
                    DataTable hccTable = ds.Tables[ProcessData.ErrorSheet];
                    hccTable.Columns.Add("AmountEx", typeof(double), "Amount");
                    object total = hccTable.Compute("SUM(AmountEx)", "");

                    hccTotal = total != null ? Convert.ToDouble(total) : 0;
                }
                if (hccTotal > 0)
                    tabSheet.get_Range("G" + (rowNum + 2), "J" + (rowNum + 2)).Value = new object[] { "", Math.Round(hccTotal, 2), "", "", "HCC" };
                else
                    tabSheet.get_Range("G" + (rowNum + 2), "J" + (rowNum + 2)).Value = new object[] { Math.Round(-hccTotal, 2), "", "", "HCC" };


            }
        }
        public /*static*/ string GetUsedRangeAddress(Excel.Worksheet xlWorkSheet)
        {
            //xlWorkSheet.Columns.ClearFormats();
            //xlWorkSheet.Rows.ClearFormats();

            //iTotalColumns = xlWorkSheet.UsedRange.Columns.Count;
            //iTotalRows = xlWorkSheet.UsedRange.Rows.Count;

            string address = null;

            Excel.Range selectedRange = xlWorkSheet.Application.Selection as Excel.Range;
            address = selectedRange.Address.Contains(":") ? selectedRange.Address : xlWorkSheet.UsedRange.Address;
            return address;
        }

        public /*static*/ void HighlightRange(string sheetName, string Addresss)
        {
            Excel.Worksheet worksheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(sheetName);
            Addresss = string.IsNullOrEmpty(Addresss) ? worksheet.UsedRange.Address : Addresss;
            Utilities.NamedRange nr = Utilities.NamedRange.ExtractNamedRanges(Addresss);
            for (int i = nr.StartRow + 1; i <= nr.EndRow; i++)
            {
                Excel.Range range = worksheet.get_Range("A" + i).EntireRow;
                range.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
            }
        }

        public /*static*/ void MoveRange(string sheetName, string Addresss)
        {
            DataTable BankTab = ExcelDataSet.Tables[sheetName];
            Excel.Worksheet worksheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(sheetName);
            Addresss = string.IsNullOrEmpty(Addresss) ? worksheet.UsedRange.Address : Addresss;
            Utilities.NamedRange nr = Utilities.NamedRange.ExtractNamedRanges(Addresss);
            Excel.Worksheet tabSheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(ProcessData.ErrorSheet);
            for (int i = nr.StartRow + 1; i <= nr.EndRow; i++)
            {
                Excel.Range cell = worksheet.get_Range("A" + i);
                if ((cell.Value != null) && (cell.Font.Color == System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red)))
                {
                    DataRow row = BankTab.Select(string.Format("RowNum='{0}'", i.ToString())).FirstOrDefault();
                    //object[] values = { row["Tran_Date"], row["Account_No"], row["Segment_Id"],row["ccy"],row["closing_bal"], row["Amount"], row["Tran_code"], row["Narrative"] }; //, dr["Prime"], dr["Sub"], dr["Cc"], dr["RowNum"] };
                    object[] values = { row["Narrative"], row["Amount"], row["Serial"], "", "", "", row["Tran_Date"], row["Account_No"], row["Segment_Id"], row["ccy"], row["closing_bal"], row["Tran_code"] };
                    Excel.Range usedRange = tabSheet.UsedRange;
                    Utilities.NamedRange used = Utilities.NamedRange.ExtractNamedRanges(usedRange.Address);
                    tabSheet.get_Range(used.StartColumn + (used.EndRow + 1), used.EndColumn + (used.EndRow + 1)).Value = values;
                    cell.EntireRow.ClearContents();
                }

            }
        }

        public /*static*/ void SetMoveTabs()
        {
            List<string> tabs = new List<string>();
            DataTable dtLogic = ExcelDataSet.Tables[ProcessData.LogicSheet];
            DataTable table = dtLogic.DefaultView.ToTable(true, Admin.TabToMove);

            foreach (DataRow row in table.Rows)
            {
                string newTabName = row[Admin.TabToMove].ToString();
                tabs.Add(newTabName);
            }
            ProcessData.MoveTabSheets = tabs;
        }
        public /*static*/ void CreateNewTabs(List<string> missingTab)
        {
            List<string> sheets = new List<string>();
            Excel.Workbook workbook = (Excel.Workbook)Globals.ThisAddIn.Application.ActiveWorkbook;
            foreach (Excel.Worksheet sheet in workbook.Sheets)
            {
                sheets.Add(sheet.Name);
            }
            try
            {
                Excel.Worksheet tabSheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(ProcessData.SampleSheet);

                tabSheet.get_Range("A1").EntireRow.Copy();
                foreach (string tab in missingTab)
                {
                    if (!sheets.Contains(tab, StringComparer.OrdinalIgnoreCase))
                    {
                        Excel.Worksheet newWorksheet = (Excel.Worksheet)Globals.ThisAddIn.Application.Worksheets.Add(After: workbook.Sheets[workbook.Sheets.Count]);
                        newWorksheet.Name = tab;
                        //newWorksheet.get_Range("A1").EntireRow.Value = tabSheet.get_Range("A1").EntireRow.Value;
                        newWorksheet.get_Range("A1").EntireRow.PasteSpecial(Excel.XlPasteType.xlPasteAll);
                        newWorksheet.get_Range("A1").EntireRow.PasteSpecial(Excel.XlPasteType.xlPasteColumnWidths);

                    }
                }

                Globals.ThisAddIn.Application.CutCopyMode = (Excel.XlCutCopyMode)0;
            }
            catch (COMException ex)
            {
                string message = string.Format("Please check if a {0} exists if not create a sheet with headings '{1}'", ProcessData.SampleSheet, string.Join(",", Admin.NewTabHeading));
                MessageBox.Show(message);
            }
        }

        public /*static*/ void CalculateJournalSummary()
        {

            double strDaysMovement = 0;

            this.UpdateAllSheetNames(ProcessData);



            foreach (string tabName in ProcessData.MoveTabSheets)
            {
                if (ProcessData.AllSheets.Contains(tabName, StringComparer.OrdinalIgnoreCase))
                {
                    Excel.Worksheet tabSheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(tabName);
                    Excel.Range usedRange2 = tabSheet.UsedRange;
                    usedRange2.BorderAround(Type.Missing, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, Type.Missing);
                    Utilities.NamedRange used2 = Utilities.NamedRange.ExtractNamedRanges(usedRange2.Address);

                    Excel.Range total = tabSheet.Cells[used2.EndRow, 1];
                    if (total != null && (total.Value == "Total" || total.Value == "DAY'S MOVEMENT"))
                    {
                        total.EntireRow.Delete(Excel.XlDeleteShiftDirection.xlShiftUp);
                    }
                    tabSheet.Cells[used2.EndRow + 1, 2].Formula = string.Format("=SUM(B2:B{0})", used2.EndRow);


                    if (tabName.IndexOf(ProcessData.ChequeSheet, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        tabSheet.Cells[used2.EndRow + 1, 1] = "DAY'S MOVEMENT";
                        strDaysMovement = tabSheet.Cells[used2.EndRow + 1, 2].Value;
                    }
                    else
                    {
                        tabSheet.Cells[used2.EndRow + 1, 1] = "Total";

                    }
                    tabSheet.UsedRange.BorderAround(Type.Missing, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, Type.Missing);
                }
            }

            Excel.Worksheet journalSheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.get_Item(ProcessData.JournalSheet);
            Excel.Range usedRange = journalSheet.UsedRange;
            //usedRange.Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Blue);

            Utilities.NamedRange used = Utilities.NamedRange.ExtractNamedRanges(usedRange.Address);

            //BALANCE//prime: 1010,sub: 00000,CC: 100000,ACT: C,CO: AUS,Div: 700
            var user = Globals.ThisAddIn.Application.UserName;
            var initials = Environment.UserName.Split(' ').ToList().Select(x => x.Substring(0, 1));
            DateTime tran_date;
            DateTime.TryParseExact(ProcessData.BankStatementDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out tran_date);
            string description = string.Format("{0}_WBC {1}", string.Join("", initials), tran_date.ToString("ddMMyy"));
            object[] balancevalues = { "1010", "00000", "100000", "C", "AUS", "700", "", "", description, "" };
            journalSheet.get_Range("A" + (used.EndRow + 1), "J" + (used.EndRow + 1)).Value = balancevalues;
            journalSheet.UsedRange.BorderAround(Type.Missing, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, Type.Missing);
            //Debit:G, Credit:H
            journalSheet.Cells[used.EndRow + 2, 7].Formula = string.Format("=SUM(G9:G{0})", used.EndRow + 1); //G
            journalSheet.Cells[used.EndRow + 2, 8].Formula = string.Format("=SUM(H9:H{0})", used.EndRow + 1); //H


            double dr = journalSheet.Cells[used.EndRow + 2, 7].Value;
            double cr = journalSheet.Cells[used.EndRow + 2, 8].Value;
            double adjustment = cr - dr;
            if (dr > cr)
            {
                journalSheet.Cells[used.EndRow + 1, 8] = dr - cr;
            }
            else
            {
                journalSheet.Cells[used.EndRow + 1, 7] = cr - dr;
            }
            journalSheet.UsedRange.BorderAround(Type.Missing, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, Type.Missing);

            journalSheet.Cells[used.EndRow + 3, 7] = "DIFFERENCE"; //G
            journalSheet.Cells[used.EndRow + 3, 8] = Math.Abs(dr - cr);
            journalSheet.Cells[used.EndRow + 4, 7] = "PRESENTED CHEQUES TOTAL "; //G
            journalSheet.Cells[used.EndRow + 4, 8] = strDaysMovement; //H
            journalSheet.Cells[used.EndRow + 5, 7] = "JOURNAL MOVEMENT"; //G
            journalSheet.Cells[used.EndRow + 5, 8].Formula = string.Format("=H{0}-H{1}", used.EndRow + 3, used.EndRow + 4); //H

            journalSheet.UsedRange.BorderAround(Type.Missing, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, Type.Missing);

            //BANK MOVEMENTS
            journalSheet.get_Range("G" + 2, "I" + 2).Value = new object[] { "PC", "", strDaysMovement };
            double bankMovement = journalSheet.get_Range("BankMovement").Value;
            journalSheet.get_Range("Variance").Value = bankMovement - adjustment;
        }
    }
}
