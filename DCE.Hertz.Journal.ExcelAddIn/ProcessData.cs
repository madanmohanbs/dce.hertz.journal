﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DCE.Hertz.Journal.ExcelAddIn
{
    public class ProcessData
    {
        public /*static*/ ProcessData()
        {
            LogicSheet = "Business Logic";
            JournalSheet = "Oracle (Journal)";
            ErrorSheet = "HCC";
            ChequeSheet = "Present CHQ";
            SummaryTabs = new List<string> { "VIS", "AMX", "DCL" };
           
        }
        public /*static*/ string LogicSheet { get; set; }
        public /*static*/ List<string> BankSheets { get; set; }
        public /*static*/ string BankSheetRange { get; set; }

        public /*static*/ List<string> AllSheets { get; set; }
        public /*static*/ List<string> MoveTabSheets { get; set; }

        public /*static*/ List<string> SummaryTabs { get; set; }

        public /*static*/ string JournalSheet { get; set; }

        public /*static*/ List<string> AttachmentSheets { get; set; }

        public /*static*/ string ChequeSheet { get; set; }
        public /*static*/ string ErrorSheet { get; set; }

        public /*static*/ string SampleSheet { get; set; }

        public /*static*/ List<string> MissingSheets { get; set; }

        public /*static*/ string BankStatementDate { get; set; }

        
    }
}
