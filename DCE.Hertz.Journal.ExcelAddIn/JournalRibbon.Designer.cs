﻿namespace DCE.Hertz.Journal.ExcelAddIn
{
    partial class JournalRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public JournalRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabJournal = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.btnTaskPane = this.Factory.CreateRibbonToggleButton();
            this.separator1 = this.Factory.CreateRibbonSeparator();
            this.btnOpenTemplate = this.Factory.CreateRibbonToggleButton();
            this.group3 = this.Factory.CreateRibbonGroup();
            this.btnValidate = this.Factory.CreateRibbonToggleButton();
            this.btnMove = this.Factory.CreateRibbonToggleButton();
            this.grpMissingTab = this.Factory.CreateRibbonGroup();
            this.btnRefreshSheets = this.Factory.CreateRibbonButton();
            this.cmbSampleSheet = this.Factory.CreateRibbonComboBox();
            this.separator2 = this.Factory.CreateRibbonSeparator();
            this.btnCreateTabs = this.Factory.CreateRibbonToggleButton();
            this.group4 = this.Factory.CreateRibbonGroup();
            this.btnJournal = this.Factory.CreateRibbonToggleButton();
            this.separator3 = this.Factory.CreateRibbonSeparator();
            this.btnMail = this.Factory.CreateRibbonToggleButton();
            this.group2 = this.Factory.CreateRibbonGroup();
            this.tabJournal.SuspendLayout();
            this.group1.SuspendLayout();
            this.group3.SuspendLayout();
            this.grpMissingTab.SuspendLayout();
            this.group4.SuspendLayout();
            this.group2.SuspendLayout();
            // 
            // tabJournal
            // 
            this.tabJournal.Groups.Add(this.group1);
            this.tabJournal.Groups.Add(this.group2);
            this.tabJournal.Groups.Add(this.grpMissingTab);
            this.tabJournal.Groups.Add(this.group3);
            this.tabJournal.Groups.Add(this.group4);
            this.tabJournal.Label = "JOURNAL";
            this.tabJournal.Name = "tabJournal";
            // 
            // group1
            // 
            this.group1.Items.Add(this.btnTaskPane);
            this.group1.Items.Add(this.separator1);
            this.group1.Items.Add(this.btnOpenTemplate);
            this.group1.Label = "Open";
            this.group1.Name = "group1";
            // 
            // btnTaskPane
            // 
            this.btnTaskPane.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnTaskPane.Image = global::DCE.Hertz.Journal.ExcelAddIn.Properties.Resources.Settings_3_64;
            this.btnTaskPane.KeyTip = "T";
            this.btnTaskPane.Label = "Task Pane";
            this.btnTaskPane.Name = "btnTaskPane";
            this.btnTaskPane.ScreenTip = "Done";
            this.btnTaskPane.ShowImage = true;
            this.btnTaskPane.SuperTip = "Completed ";
            this.btnTaskPane.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnTaskPane_Click);
            // 
            // separator1
            // 
            this.separator1.Name = "separator1";
            // 
            // btnOpenTemplate
            // 
            this.btnOpenTemplate.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnOpenTemplate.Image = global::DCE.Hertz.Journal.ExcelAddIn.Properties.Resources.template_icon;
            this.btnOpenTemplate.Label = "Load Sheet";
            this.btnOpenTemplate.Name = "btnOpenTemplate";
            this.btnOpenTemplate.ShowImage = true;
            this.btnOpenTemplate.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnOpenTemplate_Click);
            // 
            // group3
            // 
            this.group3.Items.Add(this.btnMove);
            this.group3.Label = "Process";
            this.group3.Name = "group3";
            // 
            // btnValidate
            // 
            this.btnValidate.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnValidate.Image = global::DCE.Hertz.Journal.ExcelAddIn.Properties.Resources.validate;
            this.btnValidate.Label = "Validate";
            this.btnValidate.Name = "btnValidate";
            this.btnValidate.ShowImage = true;
            this.btnValidate.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnValidate_Click);
            // 
            // btnMove
            // 
            this.btnMove.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnMove.Image = global::DCE.Hertz.Journal.ExcelAddIn.Properties.Resources.move_icon_64x64;
            this.btnMove.Label = "Move";
            this.btnMove.Name = "btnMove";
            this.btnMove.ShowImage = true;
            this.btnMove.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnMove_Click);
            // 
            // grpMissingTab
            // 
            this.grpMissingTab.Items.Add(this.btnRefreshSheets);
            this.grpMissingTab.Items.Add(this.cmbSampleSheet);
            this.grpMissingTab.Items.Add(this.separator2);
            this.grpMissingTab.Items.Add(this.btnCreateTabs);
            this.grpMissingTab.Label = "Missing Sheets";
            this.grpMissingTab.Name = "grpMissingTab";
            this.grpMissingTab.Visible = false;
            // 
            // btnRefreshSheets
            // 
            this.btnRefreshSheets.Image = global::DCE.Hertz.Journal.ExcelAddIn.Properties.Resources.refresh;
            this.btnRefreshSheets.Label = "Refresh";
            this.btnRefreshSheets.Name = "btnRefreshSheets";
            this.btnRefreshSheets.ShowImage = true;
            this.btnRefreshSheets.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnRefreshSheets_Click);
            // 
            // cmbSampleSheet
            // 
            this.cmbSampleSheet.Label = "Ref Sheet";
            this.cmbSampleSheet.Name = "cmbSampleSheet";
            this.cmbSampleSheet.Text = null;
            this.cmbSampleSheet.TextChanged += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cmbSampleSheet_TextChanged);
            // 
            // separator2
            // 
            this.separator2.Name = "separator2";
            // 
            // btnCreateTabs
            // 
            this.btnCreateTabs.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnCreateTabs.Image = global::DCE.Hertz.Journal.ExcelAddIn.Properties.Resources.DocumentsCreate64x64;
            this.btnCreateTabs.Label = "Create Missing Sheets";
            this.btnCreateTabs.Name = "btnCreateTabs";
            this.btnCreateTabs.ShowImage = true;
            this.btnCreateTabs.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnCreateTabs_Click);
            // 
            // group4
            // 
            this.group4.Items.Add(this.btnJournal);
            this.group4.Items.Add(this.separator3);
            this.group4.Items.Add(this.btnMail);
            this.group4.Label = "Create";
            this.group4.Name = "group4";
            // 
            // btnJournal
            // 
            this.btnJournal.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnJournal.Description = "Journal";
            this.btnJournal.Image = global::DCE.Hertz.Journal.ExcelAddIn.Properties.Resources.journal_icon_64x64;
            this.btnJournal.Label = "Journal";
            this.btnJournal.Name = "btnJournal";
            this.btnJournal.ShowImage = true;
            this.btnJournal.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnJournal_Click);
            // 
            // separator3
            // 
            this.separator3.Name = "separator3";
            // 
            // btnMail
            // 
            this.btnMail.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnMail.Image = global::DCE.Hertz.Journal.ExcelAddIn.Properties.Resources.mail_red2;
            this.btnMail.Label = "Mail";
            this.btnMail.Name = "btnMail";
            this.btnMail.ShowImage = true;
            this.btnMail.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnMail_Click);
            // 
            // group2
            // 
            this.group2.Items.Add(this.btnValidate);
            this.group2.Label = "Check";
            this.group2.Name = "group2";
            // 
            // JournalRibbon
            // 
            this.Name = "JournalRibbon";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.tabJournal);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.JournalRibbon_Load);
            this.tabJournal.ResumeLayout(false);
            this.tabJournal.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.group3.ResumeLayout(false);
            this.group3.PerformLayout();
            this.grpMissingTab.ResumeLayout(false);
            this.grpMissingTab.PerformLayout();
            this.group4.ResumeLayout(false);
            this.group4.PerformLayout();
            this.group2.ResumeLayout(false);
            this.group2.PerformLayout();

        }

        #endregion

        private Microsoft.Office.Tools.Ribbon.RibbonTab tabJournal;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group3;
        internal Microsoft.Office.Tools.Ribbon.RibbonToggleButton btnValidate;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator2;
        internal Microsoft.Office.Tools.Ribbon.RibbonToggleButton btnMove;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group4;
        internal Microsoft.Office.Tools.Ribbon.RibbonToggleButton btnJournal;
        internal Microsoft.Office.Tools.Ribbon.RibbonToggleButton btnOpenTemplate;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonToggleButton btnMail;
        internal Microsoft.Office.Tools.Ribbon.RibbonToggleButton btnTaskPane;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator1;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator3;
        internal Microsoft.Office.Tools.Ribbon.RibbonComboBox cmbSampleSheet;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnRefreshSheets;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpMissingTab;
        internal Microsoft.Office.Tools.Ribbon.RibbonToggleButton btnCreateTabs;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group2;
    }

    partial class ThisRibbonCollection
    {
        internal JournalRibbon JournalRibbon
        {
            get { return this.GetRibbon<JournalRibbon>(); }
        }
    }
}
