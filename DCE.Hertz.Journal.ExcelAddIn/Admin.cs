﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DCE.Hertz.Journal.ExcelAddIn
{
    public class Admin
    {

        //Business Logic Sheet
        public /*static*/ string FirstColumn = "Column Name with Unique value";
        public /*static*/ string SecondColumn = "Column Name with Unique value 2";
        public /*static*/ string FirstValue = "Unique Value";
        public /*static*/ string SecondValue = "Unique Value 2";
        public /*static*/ string TabToMove = "Tab to Move To";
        public /*static*/ string DebitFormulaColumn = "Debit Formula";
        public /*static*/ string CreditFormulaColumn = "Credit Formula";

        //Bank Sheet
        public /*static*/ string[] NewTabHeading = { "Description", "Amount", "Serial", "Prime", "Sub", "CC", "Logic Ref" };
        public /*static*/ string AmountColumn = "Amount";
        public /*static*/ string LogicRefColumn = "Logic Ref";

        public ButtonState ButtonState = new ButtonState();
        ///public /*static*/ JournalRibbon Ribbon { get; set; }
    }

    public class ButtonState
    {
        public bool btnJournalChecked = true;
        public bool btnMailChecked = true;
        public bool btnMoveChecked = true;
        public bool btnOpenTemplateChecked = true;
        public bool btnTaskPaneChecked = true;
        public bool btnValidateChecked = true;
        public bool btnCreateTabsChecked = true;
        public bool grpMissingTabVisible = false;
        public bool isMissingTabs = false;
    }
}
