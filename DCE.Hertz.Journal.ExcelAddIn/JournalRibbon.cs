﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using Excel = Microsoft.Office.Interop.Excel;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Windows.Forms;
using System.Data;
using System.Reflection;


namespace DCE.Hertz.Journal.ExcelAddIn
{
    public partial class JournalRibbon
    {
        //bool isMissingTabs = false;
        public TaskPaneControl X = null;

        private void JournalRibbon_Load(object sender, RibbonUIEventArgs e)
        {

        }

        private void btnValidate_Click(object sender, RibbonControlEventArgs e)
        {
            //try
            //{
            X.Admin.ButtonState.isMissingTabs = false;
            if (X.ExcelDataSet != null && X.ExcelDataSet.Tables.Count > 0)
            {
                foreach (string bankSheet in X.ProcessData.BankSheets)
                {
                    X.Helper.HighlightRange(bankSheet, X.ProcessData.BankSheetRange);
                    List<string> missingTabs = X.Helper.ValidateStatement(bankSheet);
                    if (missingTabs.Count > 0)
                    {
                        grpMissingTab.Visible = X.Admin.ButtonState.grpMissingTabVisible = true;
                        X.ProcessData.MissingSheets = missingTabs.Distinct().ToList();
                        string messsage = string.Join(",", X.ProcessData.MissingSheets.ToArray());
                        MessageBox.Show(messsage + " \nTabs " + (missingTabs.Count > 1 ? "are" : "is") + " missing");
                        X.Admin.ButtonState.isMissingTabs = true;
                    }
                }
                btnValidate.Checked = X.Admin.ButtonState.btnValidateChecked = false;
            }
            else
            {
                MessageBox.Show("Please load Template before Validate");
                btnValidate.Checked = X.Admin.ButtonState.btnValidateChecked = true;
            }


            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //    btnValidate.Checked = true;
            //}
        }


        private void btnMove_Click(object sender, RibbonControlEventArgs e)
        {
            //try
            //{
            if (X.Admin.ButtonState.isMissingTabs == false)
            {
                foreach (string bankSheet in X.ProcessData.BankSheets)
                {
                    X.Helper.SortStatement(bankSheet);
                    X.Helper.MoveRange(bankSheet, X.ProcessData.BankSheetRange);
                }

                btnMove.Checked = X.Admin.ButtonState.btnMoveChecked = false;
            }
            else
            {
                MessageBox.Show("Please create all missing sheets before Move");
                btnMove.Checked = X.Admin.ButtonState.btnMoveChecked = true;
            }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //    btnMove.Checked = true;
            //}
        }

        private void btnJournal_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                X.Helper.UpdateAllSheetNames(X.ProcessData);
                if (!string.IsNullOrEmpty(X.ProcessData.JournalSheet) && X.ProcessData.AllSheets.Contains(X.ProcessData.JournalSheet))
                {
                    X.Helper.CreateJournal(X.ProcessData.JournalSheet);
                    X.Helper.CalculateJournalSummary();
                    btnJournal.Checked = X.Admin.ButtonState.btnJournalChecked = false;
                }
                else
                {
                    MessageBox.Show("please selcet the 'Journal' Sheet in the task pane");
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                btnJournal.Checked = X.Admin.ButtonState.btnJournalChecked = true;
            }
        }

        private void btnTaskPane_Click(object sender, RibbonControlEventArgs e)
        {
            Microsoft.Office.Tools.CustomTaskPane ctp = Globals.ThisAddIn.CustomTaskPanes.FirstOrDefault();


            if (Globals.ThisAddIn.TaskPane != null)
            {
                bool isPaneVisible=!((RibbonToggleButton)sender).Checked;
                Globals.ThisAddIn.TaskPane.Visible = isPaneVisible;
                X.Admin.ButtonState.btnTaskPaneChecked =! isPaneVisible;

            }
            //else
            //{
            //    TaskPaneControl taskPaneControl1 = new TaskPaneControl();
            //    ctp = Globals.ThisAddIn.CustomTaskPanes.Add(taskPaneControl1, "Hertz Journal Creation");
            //}
        }

        private void btnOpenTemplate_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {

                X.ExcelDataSet = X.Helper.LoadSheet(X.ProcessData);
                if (X.ExcelDataSet.Tables.Count > 1)
                {
                    string bmessage = "";
                    X.Helper.SetMoveTabs();
                    foreach (string bankSheet in X.ProcessData.BankSheets)
                    {
                        bmessage += string.Format("{0}:{1} ", bankSheet, X.ExcelDataSet.Tables[bankSheet].Rows.Count);
                    }
                    string message = string.Format("Completed loading business logic(with {0} rows) \nand bank statement (with {1} rows)", X.ExcelDataSet.Tables[X.ProcessData.LogicSheet].Rows.Count, bmessage);
                    MessageBox.Show(message);
                    btnOpenTemplate.Checked = X.Admin.ButtonState.btnOpenTemplateChecked = false;
                }
                else
                {
                    btnOpenTemplate.Checked = X.Admin.ButtonState.btnOpenTemplateChecked = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                btnOpenTemplate.Checked = X.Admin.ButtonState.btnOpenTemplateChecked = true;
            }

        }

        private void btnMail_Click(object sender, RibbonControlEventArgs e)
        {
            if (X.ProcessData.AttachmentSheets == null)
            {
                MessageBox.Show("Please choose the attachments in the task pane");
                return;
            }
            try
            {
                Excel.Workbook activeWorkbook = Globals.ThisAddIn.Application.ActiveWorkbook;
                string tempPath = System.IO.Path.GetTempPath();
                string version = Globals.ThisAddIn.Application.Version;
                Excel.Worksheet activeWorksheet = Globals.ThisAddIn.Application.ActiveSheet;


                Outlook.Application OutlookApp = new Outlook.Application();
                Outlook.MailItem oMailItem = OutlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                Outlook.Inspector oInspector = oMailItem.GetInspector;
                //oMailItem.To = "madanmohan@datacaptureexperts.com.au";
                oMailItem.Subject = "Workbook " + activeWorkbook.Name;
                oMailItem.Body = "FYI \nSee attachments"; ;
                //oMailItem.HTMLBody = strBody;

                foreach (string sheetName in X.ProcessData.AttachmentSheets)
                {
                    string tempFile = tempPath + "\\" + sheetName + ".csv";
                    //string strBody = Helper.ExceltoHtml(activeWorksheet.Name,null);
                    string strBody = X.Helper.ExceltoCsv(sheetName, null);
                    System.IO.File.WriteAllText(tempFile, strBody);
                    oMailItem.Attachments.Add(tempFile);
                    System.IO.File.Delete(tempFile);
                }
                oMailItem.Display();
                btnMail.Checked = X.Admin.ButtonState.btnMailChecked = false;
                //oMailItem.Send();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                btnMail.Checked = X.Admin.ButtonState.btnMailChecked = true;
            }


        }

        public void ResetButtons()
        {
            btnJournal.Checked = X.Admin.ButtonState.btnJournalChecked;
            btnMail.Checked = X.Admin.ButtonState.btnMailChecked;
            btnMove.Checked = X.Admin.ButtonState.btnMoveChecked;
            btnOpenTemplate.Checked = X.Admin.ButtonState.btnOpenTemplateChecked;
            btnTaskPane.Checked = X.Admin.ButtonState.btnTaskPaneChecked;
            btnValidate.Checked = X.Admin.ButtonState.btnValidateChecked;
            btnCreateTabs.Checked = X.Admin.ButtonState.btnCreateTabsChecked;
            grpMissingTab.Visible = X.Admin.ButtonState.grpMissingTabVisible;
        }

        private void btnCreateTabs_Click(object sender, RibbonControlEventArgs e)
        {
            X.Helper.CreateNewTabs(X.ProcessData.MissingSheets);
            X.Admin.ButtonState.btnCreateTabsChecked = false;
            X.Admin.ButtonState.isMissingTabs = false;
        }

        private void cmbSampleSheet_TextChanged(object sender, RibbonControlEventArgs e)
        {
            X.ProcessData.SampleSheet = cmbSampleSheet.Text;
        }

        private void btnRefreshSheets_Click(object sender, RibbonControlEventArgs e)
        {
            cmbSampleSheet.Items.Clear();
            X.Helper.UpdateAllSheetNames(X.ProcessData);
            foreach (string sheet in X.ProcessData.AllSheets)
            {
                RibbonDropDownItem item = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                item.Label = sheet;
                cmbSampleSheet.Items.Add(item);
            }


        }
    }
}
